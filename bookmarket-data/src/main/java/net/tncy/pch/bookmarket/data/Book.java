package net.tncy.pch.bookmarket.data ;

import net.tncy.pch.validationconstraints.ISBN ;

public class Book {

    private Integer id ;
    private String title ;
    private String author ;
    private String publisher ;
    // private BookFormat format ;
    @ISBN
    private String isbn ;
    
    public Book(Integer id, String title, String author, String publisher, String isbn) {
        this.id = id ;
        this.title = title ;
        this.author = author ;
        this.publisher = publisher ;
        this.isbn = isbn ;
    }
    
}