package net.tncy.pch.bookmarket.data ;

import java.util.ArrayList ;

public class Bookstore {
    
    private Integer id ;
    private String name ;
    private ArrayList<InventoryEntry> inventoryEntries ;
    
    public Bookstore(Integer id, String name) {
        this.id = id ;
        this.name = name ;
        this.inventoryEntries = new ArrayList() ;
    }
    
}